from django import forms
from django.contrib.gis.geos import Point

from django_select2.forms import Select2MultipleWidget, Select2Widget

from ..models import Event, EventCategory
from apps.core.models import Region


class EventFilterForm(forms.ModelForm):
    event_category = forms.ModelMultipleChoiceField(required=False,
                                                    queryset=EventCategory.objects.all(),
                                                    widget=Select2MultipleWidget)
    region = forms.ModelChoiceField(required=False,
                                    queryset=Region.objects.all(),
                                    widget=Select2Widget)
    end_date = forms.CharField(required=False)
    latitude = forms.CharField(widget=forms.HiddenInput)
    longitude = forms.CharField(widget=forms.HiddenInput)

    class Meta:
        model = Event
        exclude = ('confirmed',)

    def clean(self):
        cleaned_data = self.cleaned_data
        cleaned_data['geo_position'] = Point(
            float(cleaned_data.get('longitude')),
            float(cleaned_data.get('latitude')))
        return cleaned_data

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control'
        self.fields['end_date'].widget.attrs[
            'class'] = 'form-control datetimepicker-input'
