from django import forms
from django.contrib.gis.geos import Point
from django.contrib.gis.forms import PointField

from django_select2.forms import Select2MultipleWidget

from ..models import Event, EventCategory


class CreateEventForm(forms.ModelForm):
    event_category = forms.ModelMultipleChoiceField(queryset=EventCategory.objects.all(), widget=Select2MultipleWidget)
    geo_position = PointField(required=False)
    latitude = forms.CharField(widget=forms.HiddenInput)
    longitude = forms.CharField(widget=forms.HiddenInput)

    class Meta:
        model = Event
        exclude = ('confirmed',)

    def clean(self):
        cleaned_data = self.cleaned_data
        longitude = cleaned_data.get('longitude')
        latitude = cleaned_data.get('latitude')
        if longitude and latitude:
            cleaned_data['geo_position'] = Point(float(longitude), float(latitude))
        return cleaned_data

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control'
