# Generated by Django 2.2 on 2019-08-12 03:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0003_auto_20190810_2006'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='event',
            options={'ordering': ['-id'], 'verbose_name': 'Tadbir', 'verbose_name_plural': 'Tadbirlar'},
        ),
        migrations.AlterModelManagers(
            name='event',
            managers=[
            ],
        ),
    ]
