from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView

from apps.event.forms import EventFilterForm, CreateEventForm
from .models import Event


class EventListView(ListView):
    template_name = "pages/events/events.html"
    allow_empty = True
    model = Event
    paginate_by = 4

    def get_queryset(self):
        data = Event.raw_objects.all()
        event_category = self.request.GET.getlist('event_category')
        end_date = self.request.GET.get('end_date')
        region = self.request.GET.get('region')
        if region:
            data = data.filter(neighborhood__district__region__id=region)
        if end_date:
            data = data.filter(end_date__lte=end_date)
        if event_category:
            data = data.filter(event_category__in=event_category)
        return data

    def get_context_data(self, **kwargs):
        context = super(EventListView, self).get_context_data(**kwargs)
        event_category = self.request.GET.getlist('event_category', [])
        end_date = self.request.GET.get('end_date')
        region = self.request.GET.get('region')

        context['form'] = EventFilterForm(
            initial=dict(
                event_category=event_category,
                end_date=end_date,
                region=region
            )
        )
        return context


class EventDetailView(DetailView):
    template_name = "pages/events/event_detail.html"
    model = Event

    def get_context_data(self, **kwargs):
        context = super(EventDetailView, self).get_context_data(**kwargs)
        context['latitude'] = 12
        return context


class EventCreateView(CreateView):
    template_name = "pages/events/event_create.html"
    model = Event
    form_class = CreateEventForm
    success_url = reverse_lazy('apps.event:event_list')
