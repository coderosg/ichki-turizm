from django.contrib.gis.db import models
from django.utils import timezone
from apps.core.models import BaseModel


class EventManager(models.Manager):
    def get_queryset(self):
        now = timezone.localtime(timezone.now())
        return super().get_queryset().filter(confirmed=True, end_date__gte=now)


class Event(BaseModel):
    name = models.CharField(max_length=255)
    text = models.TextField()
    address = models.CharField(max_length=255, blank=True)
    image = models.ImageField(upload_to='events/', blank=True)
    certificate = models.FileField(upload_to='events-certificate/', blank=True)
    phone = models.CharField(max_length=13, blank=True)
    email = models.EmailField(max_length=255, blank=True)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    geo_position = models.PointField()
    latitude = models.FloatField(null=True, blank=True)
    longitude = models.FloatField(null=True, blank=True)
    neighborhood = models.ManyToManyField('core.Neighborhood',
                                          related_name='event')
    source_url = models.URLField(blank=True)
    confirmed = models.BooleanField(default=False)

    event_category = models.ManyToManyField('event.EventCategory',
                                            related_name='event_category')

    objects = models.Manager()
    raw_objects = EventManager()

    class Meta:
        verbose_name = 'Tadbir'
        verbose_name_plural = 'Tadbirlar'
        ordering = ['-id']

    def __str__(self):
        return self.name


class EventCategory(BaseModel):
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name
