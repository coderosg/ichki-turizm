from django.urls import path

from apps.event.views import EventListView, EventDetailView, EventCreateView

app_name = 'apps.event'

urlpatterns = [
    path('', EventListView.as_view(), name='event_list'),
    path('<int:pk>/', EventDetailView.as_view(), name='event_detail'),
    path('create/', EventCreateView.as_view(), name='event_create'),
]
