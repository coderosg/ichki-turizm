from django.db import models
from apps.core.models import BaseModel


class TelegramUsers(BaseModel):
    user_id = models.CharField(max_length=128)
    user_name = models.CharField(max_length=128)
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    phone = models.CharField(max_length=13)
    avatar = models.FileField(blank=True, null=True, upload_to='avatars/')

    def __str__(self):
        return "%s %s" % (self.first_name, self.last_name)

    class Meta:
        ordering = ('-id',)
