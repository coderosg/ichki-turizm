from django.apps import AppConfig


class TelegramBotConfig(AppConfig):
    name = 'apps.telegram_bot'

    def ready(self):
        super().ready()
