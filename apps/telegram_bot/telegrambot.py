from .bots import bot_tour_objects
from .bots import bot_guide
from .bots import bot_transport

bot_tour_objects.main()
bot_guide.main()
bot_transport.main()
