# -*- coding: utf-8 -*-
import json
import logging
from collections import defaultdict

from .helpers import (restricted, send_typing_action, cancel_button, send_upload_photo_action)

import redis

from django.conf import settings
from django.core.paginator import Paginator
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.gis.geos import Point

from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import CommandHandler, MessageHandler, Filters, CallbackQueryHandler
from django_telegrambot.apps import DjangoTelegramBot

from apps.core.models.category import Category
from apps.core.models.images import Images
from apps.tour.models import TourObjects
from ..models.telegram import TelegramUsers

logger = logging.getLogger(__name__)
r = redis.Redis(host='localhost', port=6379, db=0)

# STEPS
STEP_OBJECT_NAME = 1
STEP_OBJECT_CATEGORY = 2
STEP_OBJECT_DESCRIPTION = 3
STEP_OBJECT_MAP = 4
STEP_OBJECT_IMAGES = 5


def clear_data(chat_id):
    return r.delete(chat_id)


def set_data(chat_id, key, value):
    old_data = json.loads(r.get(chat_id) or "{}")
    old_data[key] = value
    r.set(chat_id, json.dumps(old_data), 86400)


def get_data(chat_id, key=None):
    old_data = json.loads(r.get(chat_id))
    if key:
        return old_data.get(key)
    return old_data


@send_typing_action
@restricted
def object_name(bot, update):
    set_data(update.effective_user.id, 'stage', STEP_OBJECT_NAME)
    message = "Obyekt nomini kiriting"
    update.message.reply_text(message, reply_markup=ReplyKeyboardRemove())
    bot.sendMessage(update.effective_user.id)


@send_typing_action
@restricted
def object_category(bot, update):
    set_data(update.effective_user.id, 'stage', STEP_OBJECT_CATEGORY)
    go_back_keyboard = cancel_button()
    markup = ReplyKeyboardMarkup(go_back_keyboard, one_time_keyboard=True, resize_keyboard=True)

    category_list = Category.objects.all()
    paginator = Paginator(category_list, 2)

    page = paginator.page(1)
    keyboard = [[InlineKeyboardButton(category.name, callback_data=category.pk)] for category in page]

    if page.has_next():
        keyboard += [[InlineKeyboardButton("➡", callback_data=json.dumps({
            "page": page.next_page_number()
        }))]]

    reply_markup = InlineKeyboardMarkup(keyboard)

    update.message.reply_text('Kategoriyani tanlang ', reply_markup=reply_markup)
    update.message.reply_text('Bekor qilish uchun "orqaga qaytish tugmasini bosing" ', reply_markup=markup)
    bot.sendMessage(update.effective_user.id)


@send_typing_action
@restricted
def object_description(bot, update):
    set_data(update.effective_user.id, 'stage', STEP_OBJECT_DESCRIPTION)
    message = "Obyekt haqida ma'lumot kiriting"
    update.callback_query.message.reply_text(message)


@send_typing_action
@restricted
def object_geo_location(bot, update):
    set_data(update.effective_user.id, 'stage', STEP_OBJECT_MAP)
    message = "Obyekt joylashgan manzil koordinatasini yuboring"
    update.message.reply_text(message)
    bot.sendMessage(update.effective_user.id)


@send_typing_action
@restricted
def object_images(bot, update):
    set_data(update.effective_user.id, 'stage', STEP_OBJECT_IMAGES)
    apply_keyboard = [['Tugatish']]
    markup = ReplyKeyboardMarkup(apply_keyboard, one_time_keyboard=False, resize_keyboard=True)
    message = "Obyekt rasmlarini yuboring"
    update.message.reply_text(message, reply_markup=markup)
    bot.sendMessage(update.effective_user.id)


@restricted
def object_category_handler(bot, update):
    query = update.callback_query
    chat_id = update.effective_user.id
    data = json.loads(query.data)
    if isinstance(data, dict):
        category_list = Category.objects.all()
        paginator = Paginator(category_list, 2)
        page = paginator.page(data['page'])
        keyboard = [[InlineKeyboardButton(category.name, callback_data=category.pk)] for category in page]

        if page.has_next():
            keyboard += [[InlineKeyboardButton("➡", callback_data=json.dumps({
                "page": page.next_page_number()
            }))]]

        if page.has_previous():
            keyboard = [[InlineKeyboardButton("⬅", callback_data=json.dumps({
                "page": page.previous_page_number()
            }))]] + keyboard

        reply_markup = InlineKeyboardMarkup(keyboard)
        return query.edit_message_text('Kategoriyani tanlang', reply_markup=reply_markup)
    set_data(chat_id, 'category', query.data)
    selected_category = Category.objects.get(pk=query.data)
    query.edit_message_text("Kategoriya: {}".format(selected_category))
    object_description(bot, update)


def contact_handler(bot, update):
    if update.effective_chat.id != update.effective_message.contact.user_id:
        return update.message.reply_text("Iltimos o'zingizni telefon raqamingizni jo'nating!")
    user_id = update.effective_chat.id

    file = ''
    phone = update.effective_message.contact.phone_number
    first_name = update.effective_chat.first_name or ''
    last_name = update.effective_chat.last_name or ''
    username = update.effective_chat.username or user_id

    profile_photos = bot.get_user_profile_photos(user_id, limit=1)
    if profile_photos.total_count > 0:
        try:
            avatar = bot.get_file(profile_photos.photos[0][-1].file_id)
            file = avatar.download(custom_path='{}\\avatars\\{}_{}_{}.jpg'.format(
                settings.MEDIA_ROOT,
                first_name,
                last_name,
                user_id))
        except IndexError:
            pass

    try:
        TelegramUsers.objects.get(user_id=user_id)
    except ObjectDoesNotExist:
        user = TelegramUsers(first_name=first_name,
                             last_name=last_name,
                             user_name=username,
                             user_id=user_id,
                             phone=phone,
                             avatar=file)
        user.save()
    object_name(bot, update)


@restricted
def object_name_handler(bot, update):
    text = update.effective_message.text
    chat_id = update.effective_user.id
    set_data(chat_id, 'name', text)
    set_data(chat_id, 'stage', STEP_OBJECT_CATEGORY)
    object_category(bot, update)


@restricted
def object_description_handler(bot, update):
    text = update.effective_message.text
    chat_id = update.effective_user.id
    set_data(chat_id, 'description', text)
    set_data(chat_id, 'stage', STEP_OBJECT_MAP)
    object_geo_location(bot, update)


@restricted
def user_input_handler(bot, update):
    chat_id = update.effective_user.id
    text = update.effective_message.text
    step = get_data(chat_id, 'stage')
    if text == 'Tugatish' and step == STEP_OBJECT_IMAGES:
        return end_handler(bot, update)
    handlers = defaultdict(lambda: object_name, {
        STEP_OBJECT_NAME: object_name_handler,
        STEP_OBJECT_DESCRIPTION: object_description_handler,
        STEP_OBJECT_IMAGES: object_images,
        STEP_OBJECT_MAP: object_geo_location,
        STEP_OBJECT_CATEGORY: object_category
    })
    handlers[step](bot, update)


@restricted
def location_handler(bot, update):
    chat_id = update.effective_user.id
    set_data(chat_id, 'location', {
        "latitude": update.effective_message.location.latitude,
        "longitude": update.effective_message.location.longitude
    })
    object_images(bot, update)


@send_upload_photo_action
def images_handler(bot, update):
    chat_id = update.effective_user.id
    file_id = update.effective_message.photo[-1].file_id
    images = get_data(chat_id, 'images') or ()
    file_name = '\\tour-objects\\{}.jpg'.format(file_id)
    new_file = bot.get_file(file_id)
    new_file.download(custom_path='{}{}'.format(settings.MEDIA_ROOT, file_name))
    image = Images(image=file_name)
    image.save()
    images += (image.pk, )
    set_data(chat_id, 'images', images)


def end_handler(bot, update):
    chat_id = update.effective_user.id
    message = "Sizning so'rovingiz qabul qilindi."
    update.message.reply_text(message, reply_markup=ReplyKeyboardRemove())
    obj_data = get_data(chat_id)
    obj_images = obj_data.get('images')
    obj_longitude = obj_data.get('location').get('longitude')
    obj_latitude = obj_data.get('location').get('latitude')
    obj_name = obj_data.get('name')
    obj_description = obj_data.get('description')
    obj_category_id = obj_data.get('category')
    clear_data(chat_id)
    try:
        category = Category.objects.get(pk=obj_category_id)
        telegram_user = TelegramUsers.objects.get(user_id=chat_id)
        point = Point(obj_longitude, obj_latitude)
        tour_object = TourObjects(
            name=obj_name,
            description=obj_description,
            category=category,
            geo_position=point,
            telegram=telegram_user
        )
        tour_object.save()
        tour_object.images.add(*obj_images)
    except Exception as e:
        print(e)
    object_name(bot, update)


def error(bot, update, error):
    logger.warning('Update "%s" caused error "%s"' % (update, error))


def main():
    logger.info("Loading handlers for telegram bot")

    # Default dispatcher (this is related to the first bot in settings.TELEGRAM_BOT_TOKENS)
    dp = DjangoTelegramBot.getDispatcher('869052022:AAHD1P0cHWDLaLdQbHiPQkdBzKBZgtHYcHg')
    # To get Dispatcher related to a specific bot
    # dp = DjangoTelegramBot.getDispatcher('BOT_n_token')     #get by bot token
    # dp = DjangoTelegramBot.getDispatcher('BOT_n_username')  #get by bot username

    # start bot
    dp.add_handler(CommandHandler("start", object_name))

    # handler for all inline button
    dp.add_handler(CallbackQueryHandler(object_category_handler))

    # handler for all text
    dp.add_handler(MessageHandler(Filters.text, user_input_handler))

    # handler for location
    dp.add_handler(MessageHandler(Filters.location, location_handler))

    # handler for authentication
    dp.add_handler(MessageHandler(Filters.contact, contact_handler))

    # handler for upload images
    dp.add_handler(MessageHandler(Filters.photo, images_handler))

    # log all errors
    dp.add_error_handler(error)
