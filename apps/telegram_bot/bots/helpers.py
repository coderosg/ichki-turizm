from functools import wraps

from django.core.exceptions import ObjectDoesNotExist

from telegram import ReplyKeyboardMarkup, ChatAction

from ..models.telegram import TelegramUsers


# Wrapper for including action to bot
def send_action(action):
    """Sends `action` while processing func command."""

    def decorator(func):
        @wraps(func)
        def command_func(bot, update, *args, **kwargs):
            bot.send_chat_action(chat_id=update.effective_message.chat_id, action=action)
            return func(bot, update, *args, **kwargs)

        return command_func

    return decorator


# Wrapper for checking user is authenticated
def restricted(func):
    @wraps(func)
    def wrapped(bot, update, *args, **kwargs):
        user_id = update.effective_user['id']
        try:
            TelegramUsers.objects.get(user_id=user_id)
        except ObjectDoesNotExist:
            reply_keyboard = [[{'text': 'Ro''yxatdan o''tish', 'request_contact': True}]]
            first_name = update.effective_user['first_name']
            last_name = update.effective_user['last_name']
            message = "Assalom alaykum {} {}. Iltimos ro'yxatdan o'ting!".format(first_name, last_name)
            markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True, resize_keyboard=True)

            update.message.reply_text(message, reply_markup=markup)
            bot.sendMessage(update.message.chat_id)

            return
        return func(bot, update, *args, **kwargs)

    return wrapped


def cancel_button():
    reply_keyboard = [['Bekor qilish']]
    markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True, resize_keyboard=True)
    return markup


send_typing_action = send_action(ChatAction.TYPING)
send_upload_video_action = send_action(ChatAction.UPLOAD_VIDEO)
send_upload_photo_action = send_action(ChatAction.UPLOAD_PHOTO)
