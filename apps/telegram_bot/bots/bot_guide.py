# -*- coding: utf-8 -*-
import json
import logging
from functools import wraps
from collections import defaultdict

import redis

from django.conf import settings
from django.core.paginator import Paginator
from django.core.exceptions import ObjectDoesNotExist

from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import CommandHandler, MessageHandler, Filters, CallbackQueryHandler, DispatcherHandlerStop
from django_telegrambot.apps import DjangoTelegramBot

from apps.core.models.category import Category
from ..models.telegram import TelegramUsers

logger = logging.getLogger(__name__)
r = redis.Redis(host='localhost', port=6379, db=0)

# STEPS
START = 0

# Tour objects module steps
STEP_OBJECT_NAME = 1
STEP_OBJECT_CATEGORY = 2
STEP_OBJECT_DESCRIPTION = 3
STEP_OBJECT_MAP = 4
STEP_OBJECT_IMAGES = 5

# Guide module steps

# Transport module steps
STEP_TRANSPORT_ROLE = 6
STEP_TRANSPORT_FROM = 7
STEP_TRANSPORT_TO = 8
STEP_TRANSPORT_TRANSPORT = 9
STEP_TRANSPORT_PHONE = 10


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def restricted(func):
    @wraps(func)
    def wrapped(bot, update, *args, **kwargs):
        user_id = update.effective_user['id']
        try:
            TelegramUsers.objects.get(user_id=user_id)
        except ObjectDoesNotExist:
            reply_keyboard = [[{'text': 'Ro''yxatdan o''tish', 'request_contact': True}]]
            first_name = update.effective_user['first_name']
            last_name = update.effective_user['last_name']
            message = "Assalom alaykum {} {}. Iltimos ro'yxatdan o'ting!".format(first_name, last_name)
            markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True, resize_keyboard=True)

            update.message.reply_text(message, reply_markup=markup)
            bot.sendMessage(update.message.chat_id)

            return
        return func(bot, update, *args, **kwargs)
    return wrapped


@restricted
def start(bot, update):
    reply_keyboard = [['Gid'], ['Obyektlar'], ['Transport']]
    message = "Bo'limni tanlang"
    markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True, resize_keyboard=True)

    update.message.reply_text(message, reply_markup=markup)
    bot.sendMessage(update.message.chat_id)


# OBJECTS module
@restricted
def places(bot, update):
    categories = Category.objects.all()
    paginator = Paginator(categories, 2)

    reply_keyboard = [['Bosh menyuga qaytish']]
    markup = ReplyKeyboardRemove()

    page = paginator.page(1)
    keyboard = [[InlineKeyboardButton(category.name, callback_data=category.pk)] for category in page]

    if page.has_next():
        keyboard += [[InlineKeyboardButton("➡", callback_data=json.dumps({
            "page": page.next_page_number()
        }))]]

    reply_markup = InlineKeyboardMarkup(keyboard)

    update.message.reply_text('Kategoriyani tanlang: ', reply_markup=reply_markup)
    update.message.reply_text('', reply_markup=markup)
    bot.sendMessage(update.message.chat_id)


@restricted
def button(bot, update):
    query = update.callback_query
    data = json.loads(query.data)
    if isinstance(data, dict):
        categories = Category.objects.all()
        paginator = Paginator(categories, 2)
        page = paginator.page(data['page'])
        keyboard = [[InlineKeyboardButton(category.name, callback_data=category.pk)] for category in page]

        if page.has_next():
            keyboard += [[InlineKeyboardButton("➡", callback_data=json.dumps({
                "page": page.next_page_number()
            }))]]

        if page.has_previous():
            keyboard = [[InlineKeyboardButton("⬅", callback_data=json.dumps({
                "page": page.previous_page_number()
            }))]] + keyboard

        reply_markup = InlineKeyboardMarkup(keyboard)
        return query.edit_message_text('Kategoriyani tanlang:', reply_markup=reply_markup)

    query.edit_message_text("Tanlangan: {}".format(query.data))


def contact(bot, update):
    if update.effective_chat.id != update.effective_message.contact.user_id:
        return update.message.reply_text("Iltimos o'zingizni telefon raqamingizni jo'nating!")
    user_id = update.effective_chat.id

    file = ''
    phone = update.effective_message.contact.phone_number
    first_name = update.effective_chat.first_name
    last_name = update.effective_chat.last_name
    username = update.effective_chat.username

    profile_photos = bot.get_user_profile_photos(user_id, limit=1)
    if profile_photos.total_count > 0:
        try:
            avatar = bot.get_file(profile_photos.photos[0][-1].file_id)
            file = avatar.download(custom_path='{}\\avatars\\{}_{}_{}.jpg'.format(
                settings.MEDIA_ROOT,
                first_name,
                last_name,
                user_id))
        except IndexError:
            pass

    try:
        TelegramUsers.objects.get(user_id=user_id)
    except ObjectDoesNotExist:
        user = TelegramUsers(first_name=first_name,
                             last_name=last_name,
                             user_name=username,
                             user_id=user_id,
                             phone=phone,
                             avatar=file)
        user.save()
    start(bot, update)


@restricted
def guide(bot, update):
    update.message.reply_text("Guide")


@restricted
def objects(bot, update):
    update.message.reply_text("Objects")


@restricted
def transports(bot, update):
    update.message.reply_text("Transports")


@restricted
def text_handler(bot, update):
    chat_id = update.effective_message.chat_id
    stage = r.get("stage_{}".format(chat_id))
    text = update.effective_message.text
    if not stage:
        categories = defaultdict(lambda: start, {
            'Gid': guide,
            'Obyektlar': places,
            'Transport': transports
        })
        categories[text](bot, update)
        # button_handler(bot, update)


def button_handler(bot, update):
    r.set('foo', 'bar')


def error(bot, update, error):
    logger.warning('Update "%s" caused error "%s"' % (update, error))


def main():
    logger.info("Loading handlers for telegram bot")

    # Default dispatcher (this is related to the first bot in settings.TELEGRAM_BOT_TOKENS)
    dp = DjangoTelegramBot.getDispatcher('816216848:AAGnv0BX5iq_iKg3qVteJB4iuTvVKa26C5Y')
    # To get Dispatcher related to a specific bot
    # dp = DjangoTelegramBot.getDispatcher('BOT_n_token')     #get by bot token
    # dp = DjangoTelegramBot.getDispatcher('BOT_n_username')  #get by bot username

    # start bot
    dp.add_handler(CommandHandler("start", start))

    # handler for all inline button
    dp.add_handler(CallbackQueryHandler(button))

    # handler for all text
    dp.add_handler(MessageHandler(Filters.text, text_handler))

    # handler for authentication
    dp.add_handler(MessageHandler(Filters.contact, contact))

    # log all errors
    dp.add_error_handler(error)
