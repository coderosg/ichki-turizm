from rest_framework_simplejwt import views as jwt_views
from rest_framework.routers import DefaultRouter

from django.urls import path, include

from apps.core.api.views import RegionModelViewSet, DistrictModelViewSet, \
    NeighborhoodModelViewSet
from apps.core.views.user import MeUserView

router = DefaultRouter()
router.register('regions', RegionModelViewSet, 'regions')
router.register('districts', DistrictModelViewSet, 'districts')
router.register('neighborhoods', NeighborhoodModelViewSet, 'neighborhoods')

urlpatterns = [
    path('core/', include(router.urls)),
    path('auth/me/', MeUserView.as_view(), name='me'),
    path('auth/token/', jwt_views.TokenObtainPairView.as_view(),
         name='token_obtain_pair'),
    path('auth/token/refresh/', jwt_views.TokenRefreshView.as_view(),
         name='token_refresh'),
]
