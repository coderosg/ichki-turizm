from django.contrib.gis.db import models
from apps.core.models import BaseModel


class TourObjects(BaseModel):
    name = models.CharField(max_length=128)
    phone = models.CharField(max_length=13)
    email = models.EmailField()
    link = models.TextField()
    geo_position = models.PointField()
    description = models.TextField()
    category = models.ForeignKey('core.Category', on_delete=models.CASCADE)
    specializations = models.ManyToManyField('core.Specializations', blank=True)
    services = models.ManyToManyField('core.Services', blank=True)
    certificates = models.ManyToManyField('core.Certificates', blank=True)
    images = models.ManyToManyField('core.Images', blank=True)
    user = models.ForeignKey('core.User', null=True, on_delete=models.SET_NULL, blank=True)
    telegram = models.ForeignKey('telegram_bot.TelegramUsers', null=True, on_delete=models.SET_NULL)
    confirmed = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('-id', )


class Guide(BaseModel):
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    middle_name = models.CharField(max_length=64)
    certificate_number = models.CharField(max_length=64)
    email = models.EmailField()
    phone = models.CharField(max_length=13)
    district = models.ManyToManyField('core.District', blank=True)
    languages = models.ManyToManyField('core.Language', blank=True)
    avatar = models.FileField(blank=True, null=True, upload_to='guide/')
    user = models.ForeignKey('core.User', null=True, on_delete=models.SET_NULL)
    telegram = models.ForeignKey('telegram_bot.TelegramUsers', null=True, on_delete=models.SET_NULL, blank=True)
    confirmed = models.BooleanField(default=False)

    def __str__(self):
        return "%s %s" % (self.first_name, self.last_name)

    class Meta:
        ordering = ('-id',)


class Transport(BaseModel):
    DRIVER = 'DRIVER'
    PASSENGER = 'PASSENGER'
    ROLE = (
        (DRIVER, 'Driver'),
        (PASSENGER, 'Passenger')
    )

    automobile = models.CharField(max_length=64)
    role = models.CharField(max_length=32, choices=ROLE)
    phone = models.CharField(max_length=32)
    from_direction = models.ManyToManyField('core.Region', related_name='from_direction', blank=True)
    to_direction = models.ManyToManyField('core.Region', related_name='to_direction', blank=True)
    user = models.ForeignKey('core.User', null=True, on_delete=models.SET_NULL, blank=True)
    telegram = models.ForeignKey('telegram_bot.TelegramUsers', null=True, on_delete=models.SET_NULL, blank=True)
    confirmed = models.BooleanField(default=False)

    def __str__(self):
        return "%s -> %s" % (self.from_direction.name, self.to_direction.name)

    class Meta:
        ordering = ('-id',)
