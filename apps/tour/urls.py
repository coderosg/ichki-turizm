from django.urls import path

from apps.event.views import EventListView, EventDetailView, EventCreateView

app_name = 'apps.tour'

urlpatterns = [
    path('', EventListView.as_view(), name='tour_list'),
    path('<int:pk>/', EventDetailView.as_view(), name='tour_detail'),
    path('create/', EventCreateView.as_view(), name='tour_create'),
]
