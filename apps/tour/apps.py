from django.apps import AppConfig


class TourConfig(AppConfig):
    name = 'apps.tour'
