from apps.hotel.forms.hotel_form import CreateHotelForm

from django.views.generic import ListView, DetailView, CreateView
from django.urls import reverse_lazy

from .models import Hotel


class HotelListView(ListView):
    template_name = "pages/hotels/hotels.html"
    model = Hotel


class HotelDetailView(DetailView):
    template_name = "pages/hotels/hotel_detail.html"
    model = Hotel


class HotelCreateView(CreateView):
    template_name = "pages/hotels/hotel_create.html"
    model = Hotel
    form_class = CreateHotelForm
    success_url = reverse_lazy('apps.hotel:hotel_list')
