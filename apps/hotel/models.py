from django.contrib.gis.db import models
from django.conf import settings
from django.db.models.signals import pre_save
from telegram import Bot, ParseMode, InlineKeyboardMarkup, InlineKeyboardButton

from apps.core.models import BaseModel


class Hotel(BaseModel):
    STARS = [(i + 1, i + 1) for i in range(5)]

    name = models.CharField(max_length=255)
    phone = models.CharField(max_length=13, blank=True)
    work_phone = models.CharField(max_length=13, blank=True)
    email = models.EmailField(max_length=255, blank=True)
    address = models.CharField(max_length=255, blank=True)
    web_site = models.URLField(blank=True)
    stars = models.IntegerField(choices=STARS, null=True, blank=True)
    geo_position = models.PointField()
    hotel_type = models.ForeignKey('hotel.HotelType',
                                   on_delete=models.SET_NULL, null=True)
    services = models.ManyToManyField('hotel.Service',
                                      related_name='hotel_services',
                                      blank=True)
    images = models.ManyToManyField('hotel.HotelPhoto',
                                    related_name='hotel_images', blank=True)
    confirmed = models.BooleanField(default=False)
    published = models.BooleanField(default=False, editable=False)

    objects = models.Manager()

    def __str__(self):
        return self.name

    @staticmethod
    def publish_to_tg_group(sender, instance, **kwargs):
        token = settings.TELEGRAM_PUBLISHER_BOTS[0]['ICHKI_TURIZM_PUBLISHER']
        map_url = "https://www.google.com/maps/search/?api=1&query=%f,%f" % (
            instance.geo_position[0],
            instance.geo_position[1]
        )
        print(map_url)
        bot = Bot(token)
        chat_id = '-336488973'
        # '-336488973'
        # '-1001313389112'
        reply_markup = InlineKeyboardMarkup([
            [InlineKeyboardButton('Xaritada ochish', url=map_url)]
        ])
        published = instance.published
        confirmed = instance.confirmed
        if not published and confirmed:
            content = """Дача: %s
Адресс: %s
Веб сайт: %s
Телефон: %s""" % (
                instance.name,
                instance.address,
                instance.web_site,
                instance.phone,
            )
            photo = open(instance.images.all()[0].image.path, 'rb')
            bot.send_photo(chat_id=chat_id,
                           photo=photo,
                           caption=content,
                           reply_markup=reply_markup,
                           parse_mode=ParseMode.MARKDOWN)


pre_save.connect(Hotel.publish_to_tg_group, sender=Hotel)


class HotelType(BaseModel):
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name


class Service(models.Model):
    name = models.CharField(max_length=64)
    icon = models.FileField(blank=True, null=True, upload_to='services/')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('-id',)


class HotelPhoto(BaseModel):
    image = models.FileField(blank=True, null=True, upload_to='hotel-images/')
