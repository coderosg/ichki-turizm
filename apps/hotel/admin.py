from django.contrib import admin
from .models import Hotel, HotelType, Service, HotelPhoto

admin.site.register(Hotel)
admin.site.register(HotelType)
admin.site.register(Service)
admin.site.register(HotelPhoto)
