from django import forms
from django.contrib.gis.geos import Point

from django_select2.forms import Select2MultipleWidget, Select2Widget

from ..models import Hotel, HotelType, Service


class CreateHotelForm(forms.ModelForm):
    services = forms.ModelMultipleChoiceField(queryset=Service.objects.all(), widget=Select2MultipleWidget)
    hotel_type = forms.ModelChoiceField(queryset=HotelType.objects.all(), widget=Select2Widget)
    latitude = forms.CharField(widget=forms.HiddenInput)
    longitude = forms.CharField(widget=forms.HiddenInput)

    class Meta:
        model = Hotel
        exclude = ('confirmed',)

    def clean(self):
        cleaned_data = self.cleaned_data
        cleaned_data['geo_position'] = Point(float(cleaned_data.get('longitude')), float(cleaned_data.get('latitude')))
        return cleaned_data

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control'
