from django.urls import path

from apps.hotel.views import HotelListView, HotelDetailView, HotelCreateView

app_name = 'apps.hotel'

urlpatterns = [
    path('', HotelListView.as_view(), name='hotel_list'),
    path('<int:pk>/', HotelDetailView.as_view(), name='hotel_detail'),
    path('create/', HotelCreateView.as_view(), name='hotel_create'),
]
