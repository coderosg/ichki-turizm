from rest_framework import status
from rest_framework.views import exception_handler
from rest_framework_simplejwt.tokens import RefreshToken


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    # Now add the HTTP status code to the response.
    if response is not None and exc.status_code == status.HTTP_401_UNAUTHORIZED:
        detail = response.data.pop('detail', None)
        if detail:
            response.data['non_field_errors'] = [detail]

    return response
