from django.contrib import admin
from .models import category, place, images

admin.site.register(category.Category)
admin.site.register(place.Region)
admin.site.register(place.District)
admin.site.register(place.Neighborhood)
admin.site.register(images.Images)
