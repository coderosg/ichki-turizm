import os
import re

import xlrd
from django.conf import settings
from django.core.management.base import BaseCommand

from apps.core.models import Region, District, Neighborhood


class Command(BaseCommand):
    help = 'Run import regions from xls'

    def handle(self, *args, **options):
        regions = os.listdir(os.path.join(settings.BASE_DIR, 'data'))
        for region in regions:
            districts = os.listdir(
                os.path.join(settings.BASE_DIR, 'data', region))
            for district in districts:
                file_dir = os.path.join(settings.BASE_DIR, 'data', region,
                                        district)
                xls = xlrd.open_workbook(file_dir, formatting_info=True)
                sheet = xls.sheet_by_index(0)
                for row_num in range(sheet.nrows):
                    row = sheet.row_values(row_num)
                    for nd in row:
                        if isinstance(nd, str):
                            nd = re.sub(r'[«»”“"]', '', nd)
                            rregion = Region.objects.get_or_create(name=region)
                            dist = district.split('.')[0]
                            rdistrict = District.objects.get_or_create(
                                region=rregion[0],
                                name=dist
                            )
                            Neighborhood.objects.get_or_create(
                                district=rdistrict[0],
                                name=nd
                            )
