from rest_framework import serializers
# Project
from apps.core.models import Region, District, Neighborhood


class RegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = '__all__'


class DistrictSerializer(serializers.ModelSerializer):
    region = RegionSerializer()

    class Meta:
        model = District
        fields = '__all__'


class NeighborhoodSerializer(serializers.ModelSerializer):
    district = DistrictSerializer()

    class Meta:
        model = Neighborhood
        fields = '__all__'
