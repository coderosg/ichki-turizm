# REST framework
from rest_framework.mixins import ListModelMixin
from rest_framework.viewsets import GenericViewSet
# Project
from apps.core.api.filters import RegionsFilter, DistrictFilter, NeighborhoodFilter
from apps.core.models import Region, District, Neighborhood
from .serializers import (
    RegionSerializer,
    DistrictSerializer,
    NeighborhoodSerializer,
)


class RegionModelViewSet(ListModelMixin, GenericViewSet):
    serializer_class = RegionSerializer
    queryset = Region.objects.all()
    filter_class = RegionsFilter


class DistrictModelViewSet(ListModelMixin, GenericViewSet):
    serializer_class = DistrictSerializer
    queryset = District.objects.all()
    filter_class = DistrictFilter


class NeighborhoodModelViewSet(ListModelMixin, GenericViewSet):
    serializer_class = NeighborhoodSerializer
    queryset = Neighborhood.objects.all()
    filter_class = NeighborhoodFilter
