# Python
import django_filters

# Project
from ..models import Region, District, Neighborhood


class RegionsFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(field_name='name', lookup_expr='contains')

    class Meta:
        model = Region
        fields = []


class DistrictFilter(django_filters.FilterSet):
    class Meta:
        model = District
        fields = (
         'name', 'region')


class NeighborhoodFilter(django_filters.FilterSet):
    region_name = django_filters.CharFilter(field_name='district__region__name', lookup_expr='contains')
    name = django_filters.CharFilter(field_name='name', lookup_expr='contains')

    class Meta:
        model = Neighborhood
        fields = ['district']
