from django.db import models


class BaseModel(models.Model):
    created_date = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        abstract = True
