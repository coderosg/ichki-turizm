from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=64)
    icon = models.FileField(blank=True, null=True, upload_to='category/')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('-id',)


class Specializations(models.Model):
    name = models.CharField(max_length=64)
    icon = models.FileField(blank=True, null=True, upload_to='specializations/')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('-id',)


class Services(models.Model):
    name = models.CharField(max_length=64)
    icon = models.FileField(blank=True, null=True, upload_to='services/')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('-id',)


class Certificates(models.Model):
    name = models.CharField(max_length=64)
    icon = models.FileField(blank=True, null=True, upload_to='certificates/')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('-id',)
