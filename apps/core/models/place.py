from django.db import models


class Region(models.Model):
    name = models.CharField(max_length=64)

    objects = models.Manager

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Viloyat'
        verbose_name_plural = 'Viloyatlar'


class District(models.Model):
    name = models.CharField(max_length=64)
    region = models.ForeignKey('core.Region', on_delete=models.CASCADE)

    objects = models.Manager

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Tuman'
        verbose_name_plural = 'Tumanlar'


class Neighborhood(models.Model):
    name = models.CharField(max_length=64)
    district = models.ForeignKey('core.District', on_delete=models.CASCADE)

    objects = models.Manager

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Maxalla'
        verbose_name_plural = 'Maxallalar'
