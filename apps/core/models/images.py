from django.db import models


class Images(models.Model):
    image = models.FileField(blank=True, null=True, upload_to='images/')
