from rest_framework.generics import RetrieveAPIView
from apps.core.models import User
from apps.core.serializers.user_serializer import UserSerializer


class MeUserView(RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user
