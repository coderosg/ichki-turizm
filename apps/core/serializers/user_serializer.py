from rest_framework import serializers
from apps.core.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id', 'username', 'last_name', 'first_name'
        )
