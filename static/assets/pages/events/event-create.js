$(function () {
  $('#date-start').datetimepicker({
    format: 'YYYY-MM-DD HH:mm',
    locale: 'ru'
  });
  $('#date-end').datetimepicker({
    format: 'YYYY-MM-DD HH:mm'
  });
});

ymaps.ready(init);

function init() {
  var lat = 41.335918;
  var lng = 64.424532;
  var myPlacemark = null;

  var myMap = new ymaps.Map("map", {
    center: [lat, lng],
    zoom: 5
  });

  // Слушаем клик на карте.
  myMap.events.add('click', function (e) {
    var coords = e.get('coords');

    $('input[name="longitude"]').val(coords[1]);
    $('input[name="latitude"]').val(coords[0]);

    // Если метка уже создана – просто передвигаем ее.
    if (myPlacemark) {
      myPlacemark.geometry.setCoordinates(coords);
    }
    // Если нет – создаем.
    else {
      myPlacemark = createPlacemark(coords);
      myMap.geoObjects.add(myPlacemark);
      // Слушаем событие окончания перетаскивания на метке.
      myPlacemark.events.add('dragend', function () {
        getAddress(myPlacemark.geometry.getCoordinates());
      });
    }
    getAddress(coords);
  });

  // Создание метки.
  function createPlacemark(coords) {
    return new ymaps.Placemark(coords, {
      iconCaption: 'поиск...'
    }, {
      preset: 'islands#violetDotIconWithCaption',
      draggable: true
    });
  }

  // Определяем адрес по координатам (обратное геокодирование).
  function getAddress(coords) {
    myPlacemark.properties.set('iconCaption', 'поиск...');
    ymaps.geocode(coords).then(function (res) {
      var firstGeoObject = res.geoObjects.get(0);

      myPlacemark.properties
        .set({
          // Формируем строку с данными об объекте.
          iconCaption: [
            // Название населенного пункта или вышестоящее административно-территориальное образование.
            firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
            // Получаем путь до топонима, если метод вернул null, запрашиваем наименование здания.
            firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
          ].filter(Boolean).join(', '),
          // В качестве контента балуна задаем строку с адресом объекта.
          balloonContent: firstGeoObject.getAddressLine()
        });
      $('#id_address').val(firstGeoObject.getAddressLine());
    });
  }
}

/* ------------- Place -------------- */
const select2 = function (id, url, qparams = {}) {
  this.url = url;
  this.id = id;
  this.qparams = qparams;
  this.element = $(id);
  this.disable = function () {
    this.element.prop("disabled", true);
    this.element.val(null);
  };
  this.enable = function () {
    this.element.prop("disabled", false);
  }
};
select2.prototype.init = function (fn) {
  const {url, element, qparams} = this;
  return element.select2({
    containerCssClass: 'form-control',
    ajax: {
      url: url,
      dataType: 'json',
      data: function (params) {
        return Object.assign({
          name: params.term,
          page: params.page || 1
        }, qparams);
      },
      processResults: function (data, params) {
        params.page = params.page || 1;
        return {
          results: data
        };
      },
      cache: true
    },
    escapeMarkup: function (markup) {
      return markup;
    },
    templateResult: function (data) {
      if (data.loading) {
        return data.name;
      }
      return "<div class='select2-result-repository__statistics'>" +
        "<div class='select2-result-repository__watchers'>" + data.name + "</div>" +
        "</div>";
    },
    templateSelection: function (data) {
      return data.name || data.text;
    },
  }).on('change', fn);
};

const neighborhood = new select2('#neighborhood', '/api/v1/core/neighborhoods');
neighborhood.init();
const district = new select2('#district', '/api/v1/core/districts');
district.init(function (e) {
  neighborhood.element.select2('destroy');
  if (e.target.value) {
    neighborhood.qparams = {
      district: e.target.value
    };
    neighborhood.enable();
  } else {
    neighborhood.disable();
  }
  neighborhood.init();
});
const region = new select2('#regions', '/api/v1/core/regions');
region.init(function (e) {
  neighborhood.element.select2('destroy');
  district.element.select2('destroy');
  if (e.target.value) {
    district.qparams = {
      region: e.target.value
    };
    district.enable();
  } else {
    district.disable();
    neighborhood.disable();
  }
  district.init();
  neighborhood.init();
});
