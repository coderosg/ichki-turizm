/*!
 * Star Rating Russian Translations
 *
 * This file must be loaded after 'star-rating.js'. Patterns in braces '{}', or
 * any HTML markup tags in the messages must not be converted or translated.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 *
 * @see http://github.com/kartik-v/bootstrap-star-rating
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @author Ivan Zhuravlev.
 */
(function ($) {
    "use strict";
    $.fn.ratingLocales['uz'] = {
        defaultCaption: '{rating} Юлдузлар',
        starCaptions: {
            0.5: 'Яримта юлдуз',
            1: 'Битта юлдуз',
            1.5: 'Бир яримта юлдуз',
            2: 'Иккита юлдуз',
            2.5: 'Икки яримта юлдуз',
            3: 'Учта юлдуз',
            3.5: 'Уч яримта юлдуз',
            4: 'Тўртта юлдуз',
            4.5: 'Тўрт яримта юлдуз',
            5: 'Бешта юлдуз'
        },
        clearButtonTitle: 'Tozalash',
        clearCaption: 'Рейтинг йўқ'
    };
})(window.jQuery);
