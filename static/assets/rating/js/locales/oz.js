/*!
 * Star Rating Russian Translations
 *
 * This file must be loaded after 'star-rating.js'. Patterns in braces '{}', or
 * any HTML markup tags in the messages must not be converted or translated.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 *
 * @see http://github.com/kartik-v/bootstrap-star-rating
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @author Ivan Zhuravlev.
 */
(function ($) {
    "use strict";
    $.fn.ratingLocales['oz'] = {
        defaultCaption: '{rating} Yulduzlar',
        starCaptions: {
            0.5: 'yarimta yulduz',
            1: 'Bitta yulduz',
            1.5: 'Bir yarimta yulduz',
            2: 'Ikkita yulduz',
            2.5: 'Ikki yarimta yulduz',
            3: 'Uchta yulduz',
            3.5: 'Uch yarimta yulduz',
            4: 'To\'rtta yulduz',
            4.5: 'To\'rt yarimta yulduz',
            5: 'Beshta yulduz'
        },
        clearButtonTitle: 'Tozalash',
        clearCaption: 'Reyting yo\'q'
    };
})(window.jQuery);
