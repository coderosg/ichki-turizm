"""ichki_turizm URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from django.views.generic import TemplateView
from django.conf.urls.static import static
from django.conf import settings

from apps.core.views.views import MainPageView

urlpatterns = [
    path('admin/', admin.site.urls),

    path('', MainPageView.as_view(), name='home'),
    path('hotels/', include('apps.hotel.urls', namespace='apps.hotel')),
    path('events/', include('apps.event.urls', namespace='apps.event')),
    path('tours/', include('apps.tour.urls', namespace='apps.tour')),

    path('manifest.json',
         TemplateView.as_view(template_name='manifest.json', content_type='application/json')),
    url(r'^select2/', include('django_select2.urls')),
    # telegram bot handler
    url(r'^', include('django_telegrambot.urls')),
    path('api/v1/', include('apps.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
